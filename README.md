# Chrome

- [ ] Check out the [new clipboard access API](https://developers.google.com/web/updates/2018/03/clipboardapi)

## Running Powerful Features on insecure origins

https://stackoverflow.com/a/49888524/2715716

- [ ] Await the comment reply 

https://www.chromium.org/Home/chromium-security/deprecating-powerful-features-on-insecure-origins#TOC-Testing-Powerful-Features

```powershell
python -m SimpleHTTPServer
```

```powershell
$ip = (Get-NetIPConfiguration).IPv4Address | Where-Object {$_.InterfaceAlias -eq "Wi-Fi"} | Select-Object {$_.IPAddress}
cd "C:\Program Files (x86)\Google\Chrome\Application"
./chrome "http://$ip:8000/" -> DOMException: Only secure origins are allowed
./chrome "http://$ip:8000/" --unsafely-treat-insecure-origin-as-secure="http://$ip:8000/" -> ?
```

See the [test](index.html)

## Command line switches

https://peter.sh/experiments/chromium-command-line-switches/
